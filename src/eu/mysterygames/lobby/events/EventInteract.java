package eu.mysterygames.lobby.events;

import eu.mysterygames.lobby.core.Lobby;
import eu.mysterygames.lobby.utils.CoinChest;
import eu.mysterygames.lobby.utils.ItemBuilder;
import eu.mysterygames.lobby.utils.User;
import net.minecraft.server.v1_8_R3.BlockStairs;
import net.minecraft.server.v1_8_R3.EntityArrow;
import net.minecraft.server.v1_8_R3.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 * Created by Robin Kroczek on Jan, 2020 at 13:52
 */
public class EventInteract implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        User u = Lobby.users.get(p.getUniqueId());

        if(u.isBuild()) return;

        if(e.getClickedBlock() != null){
            if(e.getAction().equals(Action.PHYSICAL)) {
                Material m = e.getClickedBlock().getType();
                e.setCancelled(true);
                if(m.equals(Material.STONE_PLATE) || m.equals(Material.WOOD_PLATE) || m.equals(Material.IRON_PLATE) || m.equals(Material.GOLD_PLATE)) {
                    e.setUseInteractedBlock(Event.Result.DENY);
                }
            }else if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

                Material m = e.getClickedBlock().getType();
                if(!m.equals(Material.WALL_SIGN) && !m.equals(Material.SIGN) && !m.equals(Material.SIGN_POST)) {
                    e.setCancelled(true);
                    e.setUseInteractedBlock(Event.Result.DENY);


                    if(m.equals(Material.BEACON) && !u.isChest()) {
                        u.setChest(true, new CoinChest(u, e.getClickedBlock().getLocation(), 800, 4));
                        return;
                    }else if(m.equals(Material.ENDER_CHEST) && u.isChest() && !u.getCoinChest().isInAnimation()) {
                        e.getClickedBlock().setType(Material.AIR);
                        u.getCoinChest().addOpened();
                        return;
                    }else if(m.equals(Material.WOOD_STAIRS)) {
                        //sit(u, e.getClickedBlock().getLocation().add(0.5,0,0.5));
                        //return;
                    }
                }
                //    if(m.equals(Material.TRAPPED_CHEST) || m.equals(Material.CHEST) || m.equals(Material.ENDER_CHEST) || m.equals(Material.STONE_BUTTON) || m.equals(Material.WOOD_BUTTON) || m.equals(Material.LEVER) ||
                //     m.equals(Material.HOPPER) || m.equals(Material.MINECART) || m.equals(Material.STORAGE_MINECART) || m.equals(Material.FURNACE) || m.equals(Material.BURNING_FURNACE) || m.equals(Material.NOTE_BLOCK) ||
                //      m.equals(Material.ENCHANTMENT_TABLE) || m.equals(Material.WORKBENCH)) { }
            }
        }


        if(e.getItem() != null && (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && !u.isChest()) {

            if(u.animationCount != 0) return;

            ItemStack is = e.getItem();
            String dis = is.getItemMeta().getDisplayName();
            if(dis == null) return;
            if(dis.equals("§bTeleporter §7» Rechtsklick")) {
                u.fillNavInventory();
            }else if(dis.equals("§bGadgets §7» Rechtsklick")) {

            }else if(dis.equals("§cZurück §7» Rechtsklick")) {
                u.fillInventory(0, 1);
            }else if(dis.equals("§bSpawn §7» Rechtsklick")) {
                p.teleport(Lobby.spawnLoc);
                u.fillInventory(0, 1);
            }else if(dis.equals("§bBuildFFA §7» Rechtsklick")) {
                p.teleport(Lobby.bffaLoc);
                u.fillInventory(0, 1);
            }else if(dis.equals("§bDaily Chest §7» Rechtsklick")) {
                p.teleport(Lobby.dailyChestLoc);
                u.fillInventory(0, 1);
            }else if(dis.startsWith("§bAlle Spieler") || dis.startsWith("§bNur Freunde") || dis.startsWith("§bNiemanden")) {
                u.fillHideInventory();
            }else if(dis.equals("§bZeige alle Spieler §7» Rechtsklick")) {
                u.setHideOtherPlayers(0);
                u.fillInventory(0, 7);
            }else if(dis.equals("§bZeige nur Freunde §7» Rechtsklick")) {
                u.setHideOtherPlayers(1);
                u.fillInventory(0, 7);
            }else if(dis.equals("§bZeige niemanden §7» Rechtsklick")) {
                u.setHideOtherPlayers(2);
                u.fillInventory(0, 7);
            }else if(dis.equals("§bEinstellungen §7» Rechtsklick")) {
                Inventory inv = Bukkit.createInventory(null, 3*9, "§cEinstellungen");


                if(u.isHotBarNavigator()) inv.setItem(11, new ItemBuilder(Material.COMPASS).setDisplayname("§bNavigator §7➜ §aHotbar").build());
                else inv.setItem(11, new ItemBuilder(Material.COMPASS).setDisplayname("§bNavigator §7➜ §cInventar").build());

                if(u.isHotBarHidePlayers()) inv.setItem(13, new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bSpieler verstecken §7➜ §aHotbar").build());
                else inv.setItem(13, new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bSpieler verstecken §7➜ §cInventar").build());


                if(u.isHotbarAnimation()) inv.setItem(15, new ItemBuilder(Material.FISHING_ROD).setDisplayname("§bHotbar Animation §7➜ §aAktiviert").build());
                else inv.setItem(15, new ItemBuilder(Material.FISHING_ROD).setDisplayname("§bHotbar Animation §7➜ §cDeaktiviert").build());


                p.openInventory(inv);
            }
        }
    }

    private void sit(User u, Location loc) {
        u.vehicle = loc.getWorld().spawnEntity(loc, EntityType.ARROW);
        u.vehicle.setPassenger(u.getPlayer());
        //TODO: hide arrow
    }

    @EventHandler
    public void onInteractAtEntity(PlayerInteractAtEntityEvent e) {
        Player p = e.getPlayer();
        User u = Lobby.users.get(p.getUniqueId());

        if(u.isBuild()) return;

        if(e.getRightClicked() instanceof ArmorStand){
            e.setCancelled(true);
        }
    }
}
