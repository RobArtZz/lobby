package eu.mysterygames.lobby.events;

import eu.mysterygames.lobby.core.Lobby;
import eu.mysterygames.lobby.utils.MySQL;
import eu.mysterygames.lobby.utils.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:37
 */
public class EventJoinQuit implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        e.setJoinMessage(null);
        Player p = e.getPlayer();


        if(Lobby.getInstance().reconnectSQL) {
            if(MySQL.isConnected()) {
                MySQL.disconnect();
            }
            MySQL.connect();
            Lobby.getInstance().reconnectSQL = false;
        }

        User u = new User(p);
        u.join();

        Lobby.users.put(p.getUniqueId(), u);

        hide(p);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        Player p = e.getPlayer();


        if(Lobby.getInstance().reconnectSQL) {
            if(MySQL.isConnected()) {
                MySQL.disconnect();
            }
            MySQL.connect();
            Lobby.getInstance().reconnectSQL = false;
        }

        User u = Lobby.users.get(p.getUniqueId());
        u.quit();

        Lobby.users.remove(p.getUniqueId());
    }


    private void hide(Player p) {
        for(User u : Lobby.users.values()) {

            if(u.getHideOtherPlayers() == 2) {
                u.getPlayer().hidePlayer(p);
            }else if(u.getHideOtherPlayers() == 1) {
                if (u.getUserEntity().getFriendIds().contains(p.getUniqueId())) {
                    u.getPlayer().showPlayer(p);
                }else {
                    u.getPlayer().hidePlayer(p);
                }
            }else{
                u.getPlayer().showPlayer(p);
            }
        }
    }
}
