package eu.mysterygames.lobby.events;

import eu.mysterygames.lobby.core.Lobby;
import eu.mysterygames.lobby.utils.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 23:19
 */
public class EventMove implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        User u = Lobby.users.get(p.getUniqueId());

        if(u.isChest() && !u.getCoinChest().isInAnimation()) {
            if(p.getLocation().distance(u.getCoinChest().getLocation()) > 10) {
                u.getCoinChest().cancel();
            }
        }
    }
}
