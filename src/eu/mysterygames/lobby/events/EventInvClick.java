package eu.mysterygames.lobby.events;

import eu.mysterygames.lobby.core.Lobby;
import eu.mysterygames.lobby.utils.ItemBuilder;
import eu.mysterygames.lobby.utils.User;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 14:31
 */
public class EventInvClick implements Listener {

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        if(e.getWhoClicked() instanceof Player) {
            User u = Lobby.users.get(e.getWhoClicked().getUniqueId());
            Player p = u.getPlayer();
            if(u.isBuild()) return;
            e.setCancelled(true);


            if(e.getCurrentItem() != null && e.getCurrentItem().getItemMeta() != null && e.getCurrentItem().getItemMeta().getDisplayName() != null && e.getClickedInventory().getTitle() != null){
                if(e.getClickedInventory().getTitle().equals("§cEinstellungen")) {
                    String dis = e.getCurrentItem().getItemMeta().getDisplayName();

                    if(dis.startsWith("§bHotbar Animation §7➜ ")) {
                        if(u.isHotbarAnimation()){
                            e.getClickedInventory().setItem(e.getSlot(), new ItemBuilder(Material.FISHING_ROD).setDisplayname("§bHotbar Animation §7➜ §cDeaktiviert").build());
                            p.sendMessage(Lobby.prefix + "Du hast die Hotbar Animation §cdeaktiviert§7.");
                        }else{
                            e.getClickedInventory().setItem(e.getSlot(), new ItemBuilder(Material.FISHING_ROD).setDisplayname("§bHotbar Animation §7➜ §aAktiviert").build());
                            p.sendMessage(Lobby.prefix + "Du hast die Hotbar Animation §aaktiviert§7.");
                        }
                        u.setHotbarAnimation(!u.isHotbarAnimation());
                        p.updateInventory();
                    } else if (dis.startsWith("§bNavigator §7➜ ")) {
                        if(u.isHotBarNavigator()) {
                            e.getClickedInventory().setItem(e.getSlot(), new ItemBuilder(Material.COMPASS).setDisplayname("§bNavigator §7➜ §cInventar").build());
                            p.sendMessage(Lobby.prefix + "Du hast den Navigator auf das Inventar umgestellt.");
                        }else {
                            e.getClickedInventory().setItem(e.getSlot(), new ItemBuilder(Material.COMPASS).setDisplayname("§bNavigator §7➜ §aHotbar").build());
                            p.sendMessage(Lobby.prefix + "Du hast den Navigator auf die Hotbar umgestellt.");
                        }
                        u.setHotBarNavigator(!u.isHotBarNavigator());
                        p.updateInventory();

                    } else if (dis.startsWith("§bSpieler verstecken §7➜ ")) {
                        if(u.isHotBarHidePlayers()) {
                            e.getClickedInventory().setItem(e.getSlot(), new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bSpieler verstecken §7➜ §cInventar").build());
                            p.sendMessage(Lobby.prefix + "Du kannst die anderen Spieler jetzt in einem Inventar anzeigen/verstecken.");
                            p.updateInventory();
                        }else {
                            e.getClickedInventory().setItem(e.getSlot(), new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bSpieler verstecken §7➜ §aHotbar").build());
                            p.sendMessage(Lobby.prefix + "Du kannst die anderen Spieler jetzt in der Hotbar anzeigen/verstecken.");
                            p.updateInventory();
                        }
                        u.setHotBarHidePlayers(!u.isHotBarHidePlayers());
                        p.updateInventory();
                    }
                }else if(e.getClickedInventory().getTitle().equals("§bSpieler verstecken")) {
                    String dis = e.getCurrentItem().getItemMeta().getDisplayName();

                    switch (dis) {
                        case "§bZeige alle Spieler §7» Rechtsklick":
                            u.setHideOtherPlayers(0);
                            break;
                        case "§bZeige nur Freunde §7» Rechtsklick":
                            u.setHideOtherPlayers(1);
                            break;
                        case "§bZeige niemanden §7» Rechtsklick":
                            u.setHideOtherPlayers(2);
                            break;
                    }
                    p.closeInventory();
                }else if(e.getClickedInventory().getTitle().equals("§bNavigator")) {
                    String dis = e.getCurrentItem().getItemMeta().getDisplayName();

                    switch (dis) {
                        case "§bSpawn §7» Rechtsklick":
                            p.teleport(Lobby.spawnLoc);
                            p.sendMessage(Lobby.prefix + "Du bist nun am Spawn.");
                            break;
                        case "§bBuildFFA §7» Rechtsklick":
                            p.teleport(Lobby.bffaLoc);
                            p.sendMessage(Lobby.prefix + "Du bist nun bei den BuildFFA Schildern.");
                            break;
                        case "§bDaily Chest §7» Rechtsklick":
                            p.teleport(Lobby.dailyChestLoc);
                            p.sendMessage(Lobby.prefix + "Du bist nun bei der Daily-Chest.");
                            break;
                    }
                    p.closeInventory();
                }
            }
        }
    }
}
