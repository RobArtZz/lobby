package eu.mysterygames.lobby.events;

import eu.mysterygames.lobby.core.Lobby;
import eu.mysterygames.lobby.utils.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * Created by Robin Kroczek on Jan, 2020 at 18:10
 */
public class EventOther implements Listener {

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        User u = Lobby.users.get(e.getPlayer().getUniqueId());

        if(u.isBuild()) return;


        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        User u = Lobby.users.get(e.getPlayer().getUniqueId());
        if(u.isBuild()) return;

        e.setCancelled(true);
    }
    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        User u = Lobby.users.get(e.getPlayer().getUniqueId());
        if(u.isBuild()) return;

        e.setCancelled(true);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
    }
    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        e.setRespawnLocation(Lobby.spawnLoc);
    }
}
