package eu.mysterygames.lobby.core;

import eu.mysterygames.lobby.events.*;
import eu.mysterygames.lobby.utils.Config;
import eu.mysterygames.lobby.utils.MySQL;
import eu.mysterygames.lobby.utils.User;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

/**
 * Created by Robin Kroczek on Jan, 2020 at 23:44
 */
public class Lobby extends JavaPlugin {

    public static final String prefix = "§7┃ §cLobby §7» ";

    public static Lobby instance;

    public static Location spawnLoc, bffaLoc, dailyChestLoc;

    private static Config config;

    public static HashMap<UUID, User> users = new HashMap<>();

    public static List<Location> chests = new ArrayList<>();

    public boolean reconnectSQL = false;

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;
        config = new Config(this);

        register();
        connectSQL();

        Bukkit.getConsoleSender().sendMessage("Plugin: " + prefix + getName() + " enabled!");

    }
    private void connectSQL() {
        MySQL.host = "localhost";
        MySQL.database = "mysterygames";
        MySQL.username = "root";
        MySQL.password = "";
        MySQL.port = "3306";

        MySQL.connect();
        MySQL.createTables();

        Bukkit.getScheduler().runTaskTimer(getInstance(), new Runnable() {
            @Override
            public void run() {
                reconnectSQL = true;
            }
        },20 * 60 * 60 * 2,  20 * 60 * 60 * 2);
    }

    @Override
    public void onDisable() {
        super.onDisable();

        config.setList("lobby.loc.chests", chests);

        Bukkit.getConsoleSender().sendMessage("Plugin: " + prefix + getName() + " disabled!");
    }

    private void register() {

        if(config.contains("lobby.loc.spawn")) {
            spawnLoc = config.getLocation("lobby.loc.spawn");
        }else {
            Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurde noch kein Spawn-Punkt bestimmt!");
        }
        if(config.contains("lobby.loc.bffa")) {
            bffaLoc = config.getLocation("lobby.loc.bffa");
        }else {
            Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurde noch kein BuildFFA-Punkt bestimmt!");
        }

        if(config.contains("lobby.loc.dailyChest")) {
            dailyChestLoc = config.getLocation("lobby.loc.dailyChest");
        }else {
            Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurde noch kein DailyChest-Punkt bestimmt!");
        }


        if(config.contains("lobby.loc.chests")) {
            //noinspection unchecked
            chests = (List<Location>) config.getList("lobby.loc.chests");
        }else {
            Bukkit.getConsoleSender().sendMessage(prefix + "§cEs wurden noch keine Chest-Hologramm gesetzt!");
        }

        Bukkit.getPluginManager().registerEvents(new EventJoinQuit(), this);
        Bukkit.getPluginManager().registerEvents(new EventInteract(), this);
        Bukkit.getPluginManager().registerEvents(new EventDamage(), this);
        Bukkit.getPluginManager().registerEvents(new EventMove(), this);
        Bukkit.getPluginManager().registerEvents(new EventOther(), this);
        Bukkit.getPluginManager().registerEvents(new EventInvClick(), this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = null;
        User u = null;
        if(sender instanceof Player) {
            p = (Player) sender;
            u = users.get(p.getUniqueId());
        }


        if(command.getName().equalsIgnoreCase("set") && p != null && sender.hasPermission("lobby.setup")) {
            if(args.length == 1) {
                if(args[0].equalsIgnoreCase("spawn")) {
                    spawnLoc = p.getLocation();
                    config.setLocation("lobby.loc.spawn", spawnLoc);
                    p.sendMessage(prefix + "Du hast den §bSpawn §7gesetzt.");

                }else if(args[0].equalsIgnoreCase("BuildFFA")) {
                    bffaLoc = p.getLocation();
                    config.setLocation("lobby.loc.bffa", bffaLoc);
                    p.sendMessage(prefix + "Du hast die §bBuildFFA §7Location gesetzt.");

                }else if(args[0].equalsIgnoreCase("ChestHolo")) {
                    chests.add(p.getLocation());
                    p.sendMessage(prefix + "Du hast ein Chest-Hologramm gesetzt!");
                }else if(args[0].equalsIgnoreCase("Chest")) {
                    dailyChestLoc = p.getLocation();
                    config.setLocation("lobby.loc.dailyChest", dailyChestLoc);
                    p.sendMessage(prefix + "Du hast die §bChest §7Location gesetzt.");
                }

            }else {
                p.sendMessage(prefix + "Syntax: /set <Spawn/BuildFFA/Chest/ChestHolo>");
            }
        }else if(command.getName().equalsIgnoreCase("build") && p != null && sender.hasPermission("lobby.build")) {
            if(u.isBuild()) {
                u.setBuild(false);
                p.sendMessage(prefix + "Du kannst nun nicht mehr bauen.");
            }else {
                u.setBuild(true);
                p.sendMessage(prefix + "Du kannst nun bauen.");
            }
        }
        return true;
    }

    public static Lobby getInstance() {
        return instance;
    }
}
