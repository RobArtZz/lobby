package eu.mysterygames.lobby.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Config {

    YamlConfiguration cfg;
    File file;

    public Config(File file) {
        this.file = file;
        this.cfg = YamlConfiguration.loadConfiguration(file);
    }
    public Config(JavaPlugin javaPlugin){
        javaPlugin.saveDefaultConfig();
        file = new File(javaPlugin.getDataFolder() + "/config.yml");
        cfg = YamlConfiguration.loadConfiguration(file);
    }

    public String getString(String path) {
        return cfg.getString(path);
    }

    public int getInt(String path) {
        return cfg.getInt(path);
    }

    public boolean getBool(String path) {
        return cfg.getBoolean(path);
    }

    public Location getLocation(String path) {
        return new Location(Bukkit.getWorld(this.getString(path + ".world")),
                this.getDouble(path + ".x"), this.getDouble(path + ".y"), this.getDouble(path + ".z"),
                (float) this.getDouble(path + ".yaw"),(float) this.getDouble(path + ".pitch"));
    }

    public double getDouble(String path) {
        return cfg.getDouble(path);
    }

    public void setString(String path, String string) {
        cfg.set(path, string);
        this.saveConfig();
    }

    public void setInt(String path, int integer) {
        cfg.set(path, integer);
        this.saveConfig();
    }

    public void setBool(String path, boolean bool) {
        cfg.set(path, bool);
        this.saveConfig();
    }
    public void setDouble(String path, double d) {
        cfg.set(path, d);
        this.saveConfig();
    }
    public void setLocation(String path, Location loc) {
        this.setString(path + ".world", loc.getWorld().getName());
        this.setDouble(path + ".x", loc.getX());
        this.setDouble(path + ".y", loc.getY());
        this.setDouble(path + ".z", loc.getZ());
        this.setDouble(path + ".yaw", loc.getYaw());
        this.setDouble(path + ".pitch", loc.getPitch());
        this.saveConfig();
    }
    public void setList(String path, List<?> list) {
        this.cfg.set(path, list);
        saveConfig();
    }
    public List<?> getList(String path) {
        return this.cfg.getList(path);
    }
    public void setLocationList(String path, List<Location> list) {
        List<String> locs = new ArrayList<>();
        for(Location l : list) {
            locs.add(l.getWorld().getName() + " " + l.getX() + " " + l.getY() + " " + l.getZ() + " " + l.getYaw() + " " + l.getPitch());
        }
        this.cfg.set(path, locs);
        saveConfig();
    }
    public ArrayList<Location> getLocationList(String path) {
        ArrayList<Location> locs = new ArrayList<>();
        for(String s : this.cfg.getStringList(path)) {
            locs.add(new Location(Bukkit.getWorld(s.split(" ")[0]), Double.parseDouble(s.split(" ")[1]), Double.parseDouble(s.split(" ")[2]), Double.parseDouble(s.split(" ")[3]),
                    Float.parseFloat(s.split(" ")[4]), Float.parseFloat(s.split(" ")[5])));
        }
        return locs;
    }
    public boolean contains(String path) {
        if(cfg.get(path) != null) {
            return true;
        }
        return false;
    }
    private void saveConfig() {
        try {
            cfg.save(file);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
