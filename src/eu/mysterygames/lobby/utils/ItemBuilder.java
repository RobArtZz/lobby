package eu.mysterygames.lobby.utils;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

public class ItemBuilder {
	private ItemStack item;

	public ItemBuilder(Material material) {
		this.item = new ItemStack(material);
		this.item.setAmount(1);
	}

	public ItemBuilder(Material material, int amount, int data) {
		this.item = new ItemStack(material, amount, (short) (byte) data);
	}

	public ItemBuilder setDisplayname(String name) {
		ItemMeta m = this.item.getItemMeta();
		m.setDisplayName(name);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setAmount(int amount) {
		this.item.setAmount(amount);
		return this;
	}

	public ItemBuilder setLore(String... lore) {
		ItemMeta m = this.item.getItemMeta();
		m.setLore(Arrays.asList(lore));
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder addEnchant(Enchantment enchantment, int level) {
		this.item.addUnsafeEnchantment(enchantment, level);
		return this;
	}

	public ItemBuilder addFlag(ItemFlag itemFlag) {
		ItemMeta m = this.item.getItemMeta();
		m.addItemFlags(new ItemFlag[] { itemFlag });
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setUnbreakable(boolean value) {
		net.minecraft.server.v1_8_R3.ItemStack stack = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = new NBTTagCompound();
		tag.setBoolean("Unbreakable", value);
		stack.setTag(tag);
		item = CraftItemStack.asCraftMirror(stack);
		return this;
	}

	public ItemBuilder setSkullOwner(String name) {
		SkullMeta m = (SkullMeta) this.item.getItemMeta();
		m.setOwner(name);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setLeatherColor(Color color) {
		LeatherArmorMeta m = (LeatherArmorMeta) this.item.getItemMeta();
		m.setColor(color);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setBookAuthor(String author) {
		BookMeta m = (BookMeta) this.item.getItemMeta();
		m.setAuthor(author);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setBookContent(String... pages) {
		BookMeta m = (BookMeta) this.item.getItemMeta();
		m.setPages(pages);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setBookTitle(String title) {
		BookMeta m = (BookMeta) this.item.getItemMeta();
		m.setTitle(title);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemBuilder setPotionType(PotionEffectType type) {
		PotionMeta m = (PotionMeta) this.item.getItemMeta();
		m.setMainEffect(type);
		this.item.setItemMeta(m);
		return this;
	}

	public ItemStack build() {
		return this.item;
	}
}
