package eu.mysterygames.lobby.utils;

import dev.lukasl.meow.commons.entities.UserEntity;
import eu.mysterygames.lobby.core.Lobby;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_8_R3.WorldServer;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitTask;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:35
 */
public class User {

    private Player p;

    private BukkitTask task;
    public int animationCount = 0;
    //TODO: bool hidePlayers -> Api
    private boolean build = false, chest = false;
    private CoinChest coinChest = null;
    private int hideOtherPlayers = 0;

    private boolean hotbarAnimation = true, hotBarNavigator = true, hotBarHidePlayers = true;

    public Entity vehicle = null;

    private UserEntity userEntity;


    private void createGadgetEntityWeilArgsDummIst() {
        userEntity = new UserEntity();

        userEntity.setCredits(0);
        userEntity.setSelectedBoot("");
        userEntity.setSelectedEffect("");
        userEntity.setSelectedPet("");
        userEntity.setSelectedTool("");

    }

    public User(Player player) {
        this.p = player;

        createGadgetEntityWeilArgsDummIst();
    }


    public Player getPlayer() {
        return p;
    }

    public void join() {
        loadSettings();

        p.setGameMode(GameMode.ADVENTURE);
        p.teleport(Lobby.spawnLoc);

        sendScoreboard();
        spawnHolograms();

        fillInventory(10, 1);

    }

    public void quit() {
        if(task != null){
            task.cancel();
        }

        if(isChest()) {
            coinChest.cancel();
        }

        saveSettings();
    }

    private void sendScoreboard() {
      //  p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).;

        Scoreboard.sendScoreboard(p, "§6§lM§e§lystery§6§lG§e§lames.eu", ChatColor.AQUA.toString(), "§7Teamspeak: ", "§7➜ §eMysteryGames.eu", ChatColor.GRAY.toString(), "§7Coins:",
                "§7➜ §e" + getCoins(),
                ChatColor.DARK_RED.toString(), "§7Rang: ",
                "§7➜ " + (p.hasPermission("tag.admin") ? "§4Admin" :  p.hasPermission("tag.builder") ? "§2Builder" :
                        p.hasPermission("tag.dev") ? "§bDeveloper" : p.hasPermission("tag.mod") ? "§cModerator" :
                                p.hasPermission("tag.sup") ? "§9Supporter" :
                                        p.hasPermission("tag.youtuber") ? "§5Youtuber" : p.hasPermission("tag.vip") ? "§eVIP" : "§aSpieler"),
                ChatColor.DARK_GREEN.toString());

    }

    private void spawnHolograms() {

        for(Location chests : Lobby.chests) {
            WorldServer world = ((CraftWorld)chests.getWorld()).getHandle();
            EntityArmorStand hologram = new EntityArmorStand(world);
            hologram.setLocation(chests.getX(), chests.getY(), chests.getZ(), 0, 0);
            hologram.setInvisible(true);
            hologram.setCustomName("§b§lDaily Chest");
            hologram.setCustomNameVisible(true);

            PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(hologram);
            ((CraftPlayer)getPlayer()).getHandle().playerConnection.sendPacket(packet);
        }
    }

   /* public void setHidePlayers(boolean hidePlayers) {
        this.hidePlayers = hidePlayers;

        if(hidePlayers) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                getPlayer().hidePlayer(all);
            }
        }else {
            for(Player all : Bukkit.getOnlinePlayers()) {
                getPlayer().showPlayer(all);
            }
        }
    }

    public boolean isHidePlayers() {
        return hidePlayers;
    }*/

    public void fillInventory(int delay, int endSlot) {
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);

        if(isHotbarAnimation()) {
            this.task = Bukkit.getScheduler().runTaskTimerAsynchronously(Lobby.getInstance(), () -> {
                animationCount++;
                if(animationCount == 1) {
                    p.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).setDisplayname("§bTeleporter §7» Rechtsklick").build());
                    p.getInventory().setHeldItemSlot(0);
                    p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                }else if(animationCount == 2) {
                    p.getInventory().setItem(2, new ItemBuilder(Material.ENDER_CHEST).setDisplayname("§bGadgets §7» Rechtsklick").build());
                    p.getInventory().setHeldItemSlot(2);
                    p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                }else if(animationCount == 3) {
                    p.getInventory().setItem(4, new ItemBuilder(Material.NETHER_STAR).setDisplayname("§bFreunde §7» Rechtsklick").build());
                    p.getInventory().setHeldItemSlot(4);
                    p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                }else if(animationCount == 4) {
                    giveVisionItems(6);
                    p.getInventory().setHeldItemSlot(6);
                    p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                }else if(animationCount == 5) {
                    p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR).setDisplayname("§bEinstellungen §7» Rechtsklick").build());
                    p.getInventory().setHeldItemSlot(8);
                    p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                }else if(animationCount == 6) {
                    p.getInventory().setHeldItemSlot(endSlot);
                    p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);
                    animationCount = 0;
                    task.cancel();
                }
            }, delay, 7);
        }else {
            p.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).setDisplayname("§bTeleporter §7» Rechtsklick").build());
            p.getInventory().setItem(2, new ItemBuilder(Material.ENDER_CHEST).setDisplayname("§bGadgets §7» Rechtsklick").build());
            p.getInventory().setItem(4, new ItemBuilder(Material.NETHER_STAR).setDisplayname("§bFreunde §7» Rechtsklick").build());
            giveVisionItems(6);
            p.getInventory().setItem(8, new ItemBuilder(Material.REDSTONE_COMPARATOR).setDisplayname("§bEinstellungen §7» Rechtsklick").build());

        }
    }


    public void fillNavInventory() {

        if(isHotBarNavigator()) {
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);

            if(isHotbarAnimation()) {
                int heldSlot = p.getInventory().getHeldItemSlot();

                this.task = Bukkit.getScheduler().runTaskTimerAsynchronously(Lobby.getInstance(), () -> {
                    animationCount++;
                    if(animationCount == 1) {
                        p.getInventory().setItem(1, new ItemBuilder(Material.GOLD_SWORD).setUnbreakable(true).setDisplayname("§bBuildFFA §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(1);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 2) {
                        p.getInventory().setItem(3, new ItemBuilder(Material.CHEST).setDisplayname("§bDaily Chest §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(3);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 3) {
                        p.getInventory().setItem(5, new ItemBuilder(Material.GLOWSTONE_DUST).setDisplayname("§bSpawn §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(5);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 4) {
                        p.getInventory().setItem(7, new ItemBuilder(Material.BARRIER).setDisplayname("§cZurück §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(7);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 5) {
                        p.getInventory().setHeldItemSlot(heldSlot);
                        p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);
                        animationCount = 0;
                        task.cancel();
                    }
                }, 0, 7);
            }else{
                p.getInventory().setItem(1, new ItemBuilder(Material.GOLD_SWORD).setUnbreakable(true).setDisplayname("§bBuildFFA §7» Rechtsklick").build());
                p.getInventory().setItem(3, new ItemBuilder(Material.CHEST).setDisplayname("§bDaily Chest §7» Rechtsklick").build());
                p.getInventory().setItem(5, new ItemBuilder(Material.GLOWSTONE_DUST).setDisplayname("§bSpawn §7» Rechtsklick").build());
                p.getInventory().setItem(7, new ItemBuilder(Material.BARRIER).setDisplayname("§cZurück §7» Rechtsklick").build());
            }
        }else {
            Inventory inv = Bukkit.createInventory(null, 3*9,"§bNavigator");

            inv.setItem(10, new ItemBuilder(Material.GOLD_SWORD).setDisplayname("§bBuildFFA §7» Rechtsklick").build());
            inv.setItem(13, new ItemBuilder(Material.CHEST).setDisplayname("§bDaily Chest §7» Rechtsklick").build());
            inv.setItem(16, new ItemBuilder(Material.GLOWSTONE_DUST).setDisplayname("§bSpawn §7» Rechtsklick").build());

            p.openInventory(inv);
        }


    }
    public void fillHideInventory() {
        if(isHotBarHidePlayers()) {

            p.getInventory().clear();
            p.getInventory().setArmorContents(null);

            if(isHotbarAnimation()) {
                this.task = Bukkit.getScheduler().runTaskTimerAsynchronously(Lobby.getInstance(), () -> {
                    animationCount++;
                    if(animationCount == 1) {
                        p.getInventory().setItem(1, new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bZeige alle Spieler §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(1);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 2) {
                        p.getInventory().setItem(4, new ItemBuilder(Material.STICK).setDisplayname("§bZeige nur Freunde §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(4);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 3) {
                        p.getInventory().setItem(7, new ItemBuilder(Material.BONE).setDisplayname("§bZeige niemanden §7» Rechtsklick").build());
                        p.getInventory().setHeldItemSlot(7);
                        p.playSound(p.getLocation(), Sound.ITEM_PICKUP, 1.0f, 1.0f);
                    }else if(animationCount == 4) {
                        p.getInventory().setHeldItemSlot(1);
                        p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);
                        animationCount = 0;
                        task.cancel();
                    }
                }, 0, 7);
            }else {
                p.getInventory().setItem(1, new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bZeige alle Spieler §7» Rechtsklick").build());
                p.getInventory().setItem(4, new ItemBuilder(Material.STICK).setDisplayname("§bZeige nur Freunde §7» Rechtsklick").build());
                p.getInventory().setItem(7, new ItemBuilder(Material.BONE).setDisplayname("§bZeige niemanden §7» Rechtsklick").build());
            }
        }else {
            Inventory inv = Bukkit.createInventory(null, 3*9,"§bSpieler verstecken");
            inv.setItem(10, new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bZeige alle Spieler §7» Rechtsklick").build());
            inv.setItem(13, new ItemBuilder(Material.STICK).setDisplayname("§bZeige nur Freunde §7» Rechtsklick").build());
            inv.setItem(16, new ItemBuilder(Material.BONE).setDisplayname("§bZeige niemanden §7» Rechtsklick").build());
            p.openInventory(inv);
        }

    }

    public boolean isBuild() {
        return build;
    }

    public void setBuild(boolean build) {
        this.build = build;

        if(build) {
            p.setGameMode(GameMode.CREATIVE);
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
        }else {
            join();
        }
    }


    //TODO: GadgetRegistry -> getByID or getByName -> API

    public void setHideOtherPlayers(int hideOtherPlayers) {
        this.hideOtherPlayers = hideOtherPlayers;
        /*
         *
         * 0 -> Show all
         * 1 -> Show only friends
         * 2 -> hide all
         *
         */
        if(hideOtherPlayers == 2) {
            for(Player all : Bukkit.getOnlinePlayers()) {
                getPlayer().hidePlayer(all);
            }
            p.sendMessage(Lobby.prefix + "Du siehst nun niemanden mehr!");
        }else if(hideOtherPlayers == 1){
            for(Player all : Bukkit.getOnlinePlayers()) {
                if(getUserEntity().getFriendIds().contains(all.getUniqueId())) {
                    getPlayer().showPlayer(all);
                }else {
                    getPlayer().hidePlayer(all);
                }
            }
            p.sendMessage(Lobby.prefix + "Du siehst nun nur noch Freunde!");
        }else if(hideOtherPlayers == 0){
            for(Player all : Bukkit.getOnlinePlayers()) {
                getPlayer().showPlayer(all);
            }
            p.sendMessage(Lobby.prefix + "Du siehst nun alle Spieler!");
        }
    }

    public int getHideOtherPlayers() {
        return hideOtherPlayers;
    }

    public void setChest(boolean chest, CoinChest coinChest) {
        this.chest = chest;
        this.coinChest = coinChest;

        //if(chest) p.getInventory().clear();
     //   else fillInventory(0, 1);

        if(coinChest == null) return;
        coinChest.run();


    }

    public boolean isChest() {
        return chest;
    }

    public CoinChest getCoinChest() {
        return coinChest;
    }

    public void setCoins(int coins) {
        userEntity.setCredits(coins);
        sendScoreboard();

    }

    public int getCoins() {
        return userEntity.getCredits();
    }

    public void addCoins(int coins) {
        userEntity.setCredits(userEntity.getCredits() + coins);

        sendScoreboard();
    }

    public void setHotbarAnimation(boolean hotbarAnimation) {
        this.hotbarAnimation = hotbarAnimation;
    }

    public boolean isHotbarAnimation() {
        return hotbarAnimation;
    }

    public boolean isHotBarNavigator() {
        return hotBarNavigator;
    }

    public void setHotBarNavigator(boolean hotBarNavigator) {
        this.hotBarNavigator = hotBarNavigator;
    }

    public boolean isHotBarHidePlayers() {
        return hotBarHidePlayers;
    }

    public void setHotBarHidePlayers(boolean hotBarHidePlayers) {
        this.hotBarHidePlayers = hotBarHidePlayers;
    }

    private void giveVisionItems(int slot) {
        if(hideOtherPlayers == 0) p.getInventory().setItem(slot, new ItemBuilder(Material.BLAZE_ROD).setDisplayname("§bAlle Spieler §7» Rechtsklick").build());
        else if(hideOtherPlayers == 1) p.getInventory().setItem(slot, new ItemBuilder(Material.STICK).setDisplayname("§bNur Freunde §7» Rechtsklick").build());
        else if(hideOtherPlayers == 2) p.getInventory().setItem(slot, new ItemBuilder(Material.BONE).setDisplayname("§bNiemanden §7» Rechtsklick").build());
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    private void loadSettings() {

        ResultSet rs = MySQL.getResult("SELECT * FROM Lobby WHERE UUID='" + getPlayer().getUniqueId().toString() + "';");

        try {
            if(rs == null) return;
            if(rs.next()) {
                setHotBarNavigator(rs.getBoolean("Navigator"));
                setHotBarHidePlayers(rs.getBoolean("HidePlayers"));
                setHotbarAnimation(rs.getBoolean("HotbarAnimation"));
            }else {
                MySQL.uptade("INSERT INTO Lobby (UUID, Navigator, HidePlayers, HotbarAnimation) VALUES ('"+getPlayer().getUniqueId().toString()+"', "+isHotBarNavigator()+", "
                        +isHotBarHidePlayers()+", "+isHotbarAnimation()+");");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveSettings() {
        MySQL.uptade("UPDATE Lobby SET Navigator='"+(isHotBarNavigator() ? 1 : 0)+"', HidePlayers='"+(isHotBarHidePlayers() ? 1 : 0)+"', HotbarAnimation='"+(isHotbarAnimation() ? 1 : 0)
                +"' WHERE UUID='"+getPlayer().getUniqueId().toString()+"';");
    }

}
