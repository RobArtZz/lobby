package eu.mysterygames.lobby.utils;

import eu.mysterygames.lobby.core.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.Random;

/**
 * Created by Robin Kroczek on Jan, 2020 at 00:13
 */
public class CoinChest {

    public static Random random = new Random();

    public User owner;
    public Player player;
    public Location location;

    private BukkitTask task;
    private int animation = 0;
    private int opened = 0;
    private boolean inAnimation = true, ended = false;

    private int maxOpenings;
    private int maxWin;
    private int winPerChest;
    private int win = 0;

    public CoinChest(User owner, Location location, int maxWin, int maxOpenings) {
        this.owner = owner;
        this.location = location;
        this.player = owner.getPlayer();


        //TODO: monthly/weekly coin booster in Hand to activate (Monthly -> maxWin * 4 | Weekly -> maxWin * 2)
        this.maxWin = maxWin;
        this.maxOpenings = maxOpenings;
        this.winPerChest = maxWin / maxOpenings;

        getPlayer().teleport(location.clone().add(0.5,0,0.5));
    }

    public void run() {
        ended = false;
        getLocation().getBlock().setType(Material.AIR);
        getPlayer().playSound(getPlayer().getLocation(), Sound.ANVIL_LAND, 1f, 1f);

        this.task = Bukkit.getScheduler().runTaskTimer(Lobby.getInstance(), new Runnable() {
            @Override
            public void run() {
                animation++;

                if(animation == 1) {
                    inAnimation = true;
                    getLocation().clone().add(0, 1, 3).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 2) {
                    getLocation().clone().add(2, 1, 2).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 3) {
                    getLocation().clone().add(3, 1, 0).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 4) {
                    getLocation().clone().add(2, 1, -2).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 5) {
                    getLocation().clone().add(0, 1, -3).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 6) {
                    getLocation().clone().add(-2, 1, -2).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 7) {
                    getLocation().clone().add(-3, 1, 0).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                }else if(animation == 8) {
                    getLocation().clone().add(-2, 1, 2).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.ITEM_PICKUP, 1f, 1f);
                    animation = 0;
                    inAnimation = false;

                    if(getPlayer().getLocation().distance(getLocation()) > 10) {
                        cancel();
                    }

                    task.cancel();

                    runCancelTimer();
                }


               /* if(animation == 1) {
                    inAnimation = true;
                    getLocation().clone().add(0, 1, 3).getBlock().setType(Material.ENDER_CHEST);
                    getLocation().clone().add(0, 1, -3).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                }else if(animation == 2) {
                    getLocation().clone().add(3, 1, 0).getBlock().setType(Material.ENDER_CHEST);
                    getLocation().clone().add(-3, 1, 0).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                }else if(animation == 3) {
                    getLocation().clone().add(2, 1, 2).getBlock().setType(Material.ENDER_CHEST);
                    getLocation().clone().add(-2, 1, -2).getBlock().setType(Material.ENDER_CHEST);
                    getLocation().clone().add(2, 1, -2).getBlock().setType(Material.ENDER_CHEST);
                    getLocation().clone().add(-2, 1, 2).getBlock().setType(Material.ENDER_CHEST);
                    getPlayer().playSound(getPlayer().getLocation(), Sound.NOTE_BASS, 1f, 1f);
                }else if(animation == 4) {
                    getPlayer().playSound(getPlayer().getLocation(), Sound.NOTE_PLING, 1f, 1f);
                    animation = 0;
                    inAnimation = false;

                    if(getPlayer().getLocation().distance(getLocation()) > 10) {
                        cancel();
                    }

                    task.cancel();

                    runCancelTimer();
                }*/

            }
       // }, 20, 20);
        }, 6, 6);

    }

    public void runCancelTimer() {
        task = Bukkit.getScheduler().runTaskLater(Lobby.getInstance(), new Runnable() {
            @Override
            public void run() {
                task.cancel();
                if(!ended) {
                    cancel();
                    getPlayer().teleport(getLocation().clone().add(0.5,1,0.5));
                }
            }
        }, 20 * 15);
    }

    public void cancel() {
        getLocation().clone().add(3, 1, 0).getBlock().setType(Material.AIR);
        getLocation().clone().add(-3, 1, 0).getBlock().setType(Material.AIR);
        getLocation().clone().add(0, 1, 3).getBlock().setType(Material.AIR);
        getLocation().clone().add(0, 1, -3).getBlock().setType(Material.AIR);
        getLocation().clone().add(2, 1, 2).getBlock().setType(Material.AIR);
        getLocation().clone().add(-2, 1, -2).getBlock().setType(Material.AIR);
        getLocation().clone().add(2, 1, -2).getBlock().setType(Material.AIR);
        getLocation().clone().add(-2, 1, 2).getBlock().setType(Material.AIR);

        getLocation().getBlock().setType(Material.BEACON);


        if(opened != maxOpenings) {
            int left = maxOpenings - opened;
            int win = random.nextInt(winPerChest * left);
            owner.addCoins(win);
            getPlayer().sendMessage(Lobby.prefix + "Du hast insgesamt §a" + (win + this.win) + " §7Coins gewonnen!");
        }else {
            getPlayer().sendMessage(Lobby.prefix + "Du hast insgesamt §a" + this.win + " §7Coins gewonnen!");
            getPlayer().teleport(getLocation().clone().add(0.5,1,0.5));
        }
        getPlayer().playSound(getPlayer().getLocation(), Sound.WITHER_DEATH, 1f, 1f);

        //TODO: update Scoreboard after coin change

        task.cancel();

        getOwner().setChest(false, null);

        ended = true;
    }

    public User getOwner() {
        return owner;
    }

    public Player getPlayer() {
        return player;
    }

    public Location getLocation() {
        return location;
    }

    public void addOpened() {
        opened += 1;

        int win = random.nextInt(winPerChest);
        if(win == 0) win = 1;
        owner.addCoins(win);
        getPlayer().sendMessage(Lobby.prefix + "Du hast §a" + win + " §7Coins gewonnen!");
        getPlayer().playSound(getPlayer().getLocation(), Sound.NOTE_PLING, 1f, 1f);
        this.win += win;

        if(opened == maxOpenings) {
            cancel();
        }
    }

    public int getMaxWin() {
        return maxWin;
    }

    public int getWinPerChest() {
        return winPerChest;
    }

    public boolean isInAnimation() {
        return inAnimation;
    }

    public void setMaxWin(int maxWin) {
        this.maxWin = maxWin;
        this.winPerChest = maxWin / maxOpenings;
    }
}
