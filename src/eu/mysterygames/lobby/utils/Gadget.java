package eu.mysterygames.lobby.utils;

import org.bukkit.event.Event;

import java.util.UUID;

/**
 * Created by Robin Kroczek on Jan, 2020 at 23:12
 */
public abstract class Gadget {

    private UUID id;
    private String displayName;
    private String name;

    public Gadget(UUID id, String displayName, String name) {
        this.id = id;
        this.displayName = displayName;
        this.name = name;
    }

    abstract void animate(Event e);

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
