package eu.mysterygames.lobby.utils;

import eu.mysterygames.lobby.core.Lobby;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {
	public static String username;
	public static String password;
	public static String database;
	public static String host;
	public static String port;
	public static Connection con;
	
	
	public static void connect() {
		if(!isConnected()) {
			try {
				con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
				Bukkit.getConsoleSender().sendMessage(Lobby.prefix + "§aMySQL Verbindung geöffnet!");
			}catch(SQLException e) {
				e.printStackTrace();
				Bukkit.getConsoleSender().sendMessage(Lobby.prefix + "§cFehler beim Verbinden von MySQL!");
			}
		}
	}
	public static boolean isConnected() {
		try {
			if(con == null || con.isClosed()) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	public static void disconnect() {
		if(isConnected()) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Bukkit.getConsoleSender().sendMessage(Lobby.prefix + "§cMySQL Verbindung geschlossen!");
		}
	}
	public static void createTables() {
		if(isConnected()) {
			try {
				con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Lobby (id INT PRIMARY KEY AUTO_INCREMENT, UUID VARCHAR(100), Navigator BOOLEAN, HidePlayers BOOLEAN, HotbarAnimation BOOLEAN);");
			} catch (SQLException e) {
				e.printStackTrace();
				Bukkit.getConsoleSender().sendMessage(Lobby.prefix + "§cFehler beim erstellen der SQL-Tables!");
			}
		}
	}
	public static void uptade(String qry) {
		if(isConnected()) {
			try {
				con.createStatement().executeUpdate(qry);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public static ResultSet getResult(String qry) {
		if(isConnected()) {
			try {
				return con.createStatement().executeQuery(qry);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
