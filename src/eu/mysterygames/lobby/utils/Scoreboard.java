package eu.mysterygames.lobby.utils;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by RobArtZz on Dez, 2018 at 21:35
 */
public class Scoreboard {

    public static void sendScoreboard(Player p, String displayname, String... values) {

        net.minecraft.server.v1_8_R3.Scoreboard s = new net.minecraft.server.v1_8_R3.Scoreboard();
        ScoreboardObjective obj = s.registerObjective("Objective", IScoreboardCriteria.b);
        obj.setDisplayName(displayname);

        PacketPlayOutScoreboardObjective remove = new PacketPlayOutScoreboardObjective(obj, 1);
        PacketPlayOutScoreboardObjective add = new PacketPlayOutScoreboardObjective(obj, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);


        sendPacket(p, remove);
        sendPacket(p, add);
        sendPacket(p, display);

        List<String> list = Arrays.asList(values);
        Collections.reverse(list);

        for(int i = 0; i<list.size(); i++) {
            sendPacket(p, newScore(s, obj, i, list.get(i)));
        }
    }


    private static void sendPacket(Player p, Packet<?> packet) {
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
    }
    private static PacketPlayOutScoreboardScore newScore(net.minecraft.server.v1_8_R3.Scoreboard s, ScoreboardObjective obj, int score, String msg) {
        ScoreboardScore ss = new ScoreboardScore(s, obj, msg);
        ss.setScore(score);
        return new PacketPlayOutScoreboardScore(ss);
    }
}
