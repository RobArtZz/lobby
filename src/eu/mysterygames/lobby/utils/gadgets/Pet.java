package eu.mysterygames.lobby.utils.gadgets;

import eu.mysterygames.lobby.utils.User;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

/**
 * Created by Robin Kroczek on Jan, 2020 at 14:11
 */
public class Pet {

    private GadgetType type;
    private String name;
    private String displayName;
    private User owner;
    private EntityType entityType;
    protected Entity entity;

    public Pet(User owner, String name, String displayName, EntityType entityType) {
        //TODO: TYPE -> API
        this.type = GadgetType.PET;
        this.name = name;
        this.owner = owner;
    }

    public void spawnEntity() {
        World world = getOwner().getPlayer().getWorld();
        this.entity = world.spawnEntity(getOwner().getPlayer().getLocation(), entityType);

        if(!this.getDisplayName().isEmpty()) {
            this.entity.setCustomName(this.getDisplayName());
            this.entity.setCustomNameVisible(true);
        }
    }

    public void despawnEntity() {
        this.entity.remove();
    }


    public GadgetType getType() {
        return type;
    }

    public void setType(GadgetType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }
}
