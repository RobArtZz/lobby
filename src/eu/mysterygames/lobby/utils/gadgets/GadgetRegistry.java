package eu.mysterygames.lobby.utils.gadgets;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Robin Kroczek on Jan, 2020 at 23:21
 */
public class GadgetRegistry {

    private ArrayList<Gadget> gadgets;

    public GadgetRegistry() {
        gadgets = new ArrayList<>();

    }

    public ArrayList<Gadget> getGadgets() {
        return gadgets;
    }

    public Gadget getGadgetById(UUID id) {
        for(Gadget gadget : gadgets) {
            if(gadget.getId().equals(id)) return gadget;
        }
        return null;
    }
    public Gadget getGadgetByName(String name) {
        for(Gadget gadget : gadgets) {
            if(gadget.getName().equals(name)) return gadget;
        }
        return null;
    }
}
