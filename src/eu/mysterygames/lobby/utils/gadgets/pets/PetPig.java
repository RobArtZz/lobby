package eu.mysterygames.lobby.utils.gadgets.pets;

import eu.mysterygames.lobby.utils.User;
import eu.mysterygames.lobby.utils.gadgets.Pet;
import org.bukkit.entity.EntityType;

/**
 * Created by Robin Kroczek on Jan, 2020 at 14:15
 */
public class PetPig extends Pet {

    public PetPig(User owner, String displayName) {
        super(owner, "Pig", displayName, EntityType.PIG);
    }

    public PetPig(User owner) {
        super(owner, "Pig", "", EntityType.PIG);
    }
}
